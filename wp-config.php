<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'aZl$}9&ds@r1(=(J_@oO`-NH8l*b-:=drU+FN||X5Er.&Kn:?~CmYrC#);T =dlz');
define('SECURE_AUTH_KEY',  'b?Hz~1wb;^3oJy d[^XM SN4]-]hTjGHH4KSo4Z(h1Ez6Mp1eU`#-C^$2-JVs+ab');
define('LOGGED_IN_KEY',    'djy_o=IMPg.-O|<ZQb|7fl,jCMLxELZ%hW]3(a&U?$nIPWm@O7WfaD=yJ>NuprIP');
define('NONCE_KEY',        'hqA@%rD@3_.5{k=x/y!?7`9LY[By%L|bs).Eeu%RDgC8eoHu^g:-.O(RT9u^E^I(');
define('AUTH_SALT',        '3Fp7|~fU0TkkY%C691W8-J%I| DUZVS6tr?mKWJSH$][_df+.ao]xIoB){gP>W$$');
define('SECURE_AUTH_SALT', 's*WL9K<t$.9T; 5.?-)Lp[j=2YY5M*%8coqv,A7zE?3?@|Nm22kpKPP:de]azlO5');
define('LOGGED_IN_SALT',   '1M@V8*R^zo^mJddxa*0|Rb3-}.,xI-y.ujs+%-y6CVNb?:K<7|K~C|IYsgkw-7N4');
define('NONCE_SALT',       '~V]6t`oA!*i zLnP+8[5{9gR|8%*2j671sK3t4 #bQbid<Yo>f6OS[~|G*~5]0hM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
