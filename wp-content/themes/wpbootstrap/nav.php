<div class="title row">
    <a href="/">
        
    <p class="site-title">KIHOUTEI</p>
</div>
</a>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <?php
        wp_nav_menu( array(
                'menu'              => 'primary',
                'theme_location'    => 'primary',
                'depth'             => 4,
                'container'         => 'false',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav navbar-leftw',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
        );
?>
        <?php $logged = is_user_logged_in();
        if ($logged) {
            $href = '/wp-admin/';
            $text = 'Account';
        } else {
            $href = wp_login_url();
            $text = 'Login';
        }; ?>

        <ul class="nav navbar-nav navbar-right">
            <li class="Dropdown"><a href="<?php echo $href;?>"><?php echo $text;?></a>
                <?php if (!$logged) :?>
            <li><?php wp_register(); ?></li>
            <? endif ?>
            <?php if ($logged) :?>
                <li><a href="<?php echo wp_logout_url('/');?>">Logout</a>
            </li>
            <?php endif ?>


        </ul>
        </div>
    </div>
</nav>



